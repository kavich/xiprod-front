import { RouteConfig } from 'vue-router';
import Dealer from '../pages/Dealer.vue'
import Strategy from '../pages/Strategy.vue'
const routes: RouteConfig[] = [
  {
    path: '/',
    component: () => import('src/layouts/MainLayout.vue'),
    children: [
      {
        path: '',
        props: { indealerID: '35b8d117-b9b6-40ea-9393-cad970ef5c4f' },
        component: () => import('src/pages/Dealer.vue')
      }
    ]
  },
  {
    path: '/dealers/:id',

    component: () => import('src/layouts/MainLayout.vue'),
    children: [
      {

        path: '',
        component: () => import('src/pages/Dealer.vue')
      }
    ]
  },
  ,
  {
    path: '/strategy/:id',

    component: () => import('src/layouts/MainLayout.vue'),
    children: [
      {

        path: '',
        component: () => import('src/pages/Strategy.vue')
      }
    ]
  },
  {
    path: '/settings',
    component: () => import('src/layouts/SettingsLayout.vue'),
    children: [
      {
        path: '',
        props: { indealerID: '35b8d117-b9b6-40ea-9393-cad970ef5c4f' },
        component: () => import('src/pages/Settings.vue')
      }
    ]
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
];

export default routes;
